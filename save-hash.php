<?php

function saveHash($hashPassword) {
    $servername = "localhost";
    $username = "user1";
    $password = "user1strongP@ssword";
    $dbname = "hashPasswords";

    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "INSERT INTO hashPasswords (hashPassword) VALUES ('$hashPassword')";
    $result = "";

    if ($conn->query($sql) === TRUE) {
        $result = "Hash password was saved to database.";
    } else {
        $result = "Something went wrong. Please try again.";
    }

    $conn->close();

    return $result;
}

echo saveHash(trim($_POST["hashPassword"]));
