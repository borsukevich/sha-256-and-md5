<?php

function getHashes() {
    $servername = "localhost";
    $username = "user1";
    $password = "user1strongP@ssword";
    $dbname = "hashPasswords";

    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT hashPassword FROM hashPasswords";
    $result = $conn->query($sql);
    $hashes = "";

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $hashes .= $row['hashPassword'] . ",";
        }
    }

    $conn->close();

    return $hashes;
}

echo getHashes();
